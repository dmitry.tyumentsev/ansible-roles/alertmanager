Alertmanager role
=========

Installs [Alertmanager](https://github.com/prometheus/alertmanager) on Fedora, Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.alertmanager
```
